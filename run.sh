#!/bin/sh -e

NUM_OF_SLAVE=50
BASE_PORT=60000

docker ps -a | awk '$2 ~ "redis.*" { print $1 }' | xargs -I{} docker rm -f {}

docker run --name redis-master -d -p 6379:6379 redis-master

for i in `seq 1 ${NUM_OF_SLAVE}`
do
  docker run -d -p $(expr ${BASE_PORT} + ${i}):6379 --name redis-slave${i} --link redis-master redis-slave
done
